/**
 * 
 */
$(function() {
	// 参数代码
	$('#parametersCode_add').validatebox({
		required : true,
		validType : [ "length[1,100]", "not_chinese" ]
	});

	// 参数取值
	$('#parametersValue_add').validatebox({
		required : true,
		validType : [ "length[1,250]" ]
	});

	// 参数描述
	$('#parametersDesc_add').validatebox({
		required : true,
		validType : [ "length[1,250]" ]
	});

	// 保存按钮
	$('#parameters_add_btn').linkbutton({});

	// 绑定保存按钮事件
	$('#parameters_add_btn').click(function() {
		save();
	});

	// 保存方法
	function save() {
		alert('保存...');
	}
});
