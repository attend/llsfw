/**
 * 
 */
$(function() {

	// 参数取值
	$('#parametersValue_edit').validatebox({
		required : true,
		validType : [ "length[1,250]" ]
	});

	// 参数描述
	$('#parametersDesc_edit').validatebox({
		required : true,
		validType : [ "length[1,250]" ]
	});

	// 保存按钮
	$('#parameters_edit_btn').linkbutton({});

	// 绑定保存按钮事件
	$('#parameters_edit_btn').click(function() {
		save();
	});

	// 保存方法
	function save() {
		alert('修改....');
	}
});
